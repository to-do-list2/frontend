import React, { Component } from "react";
import axios from "axios";

class CreateNote extends Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],
      userSelected: "",
      title: "",
      content: ""
    }
  }

  
 
  async componentDidMount() {
  const res = await axios.get("http://localhost:4000/api/users");
    this.setState({
      users: res.data.map(user => user.username)
    })
if(this.props.match.params){
  const res = await axios.get(`http://localhost:4000/api/notes/${this.props.match.params.id}`);
 this.setState({
  userSelected: res.data.author,
  content: res.data.content,
  title: res.data.title
});

}
}

  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = async e => {
    e.preventDefault();
    const newNote = {
      title: this.state.title,
      content: this.state.content,
      author: this.state.userSelected
    };
  if(this.props.match.params.id){
    await axios.put(`http://localhost:4000/api/notes/${this.props.match.params.id}`, newNote);
  }else{
    await axios.post("http://localhost:4000/api/notes", newNote);
  }
  window.location.href = "/";

  };

  render() {
    return (
      <div className="col-md-6 offset-md-3">
        <div className="card card-body">
          {this.props.match.params.id ? <h4>Actualizar Nota</h4> : <h4>Crear Nota</h4>}
          <div className="form-group">
            <select
              className="form-control"
              name="userSelected"
              value={this.state.userSelected}

              onChange={this.handleOnChange}
            >
              {this.state.users.map(user => (
                <option key={user} value={user}>
                  {user}
                </option>
              ))}
            </select>
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              placeholder="titulo"
              name="title"
              required
              value={this.state.title}
              onChange={this.handleOnChange}
            ></input>
          </div>
          <div className="form-group">
            <textarea
              name="content"
              className="form-control"
              placeholder="content"
              value={this.state.content}
              required
              onChange={this.handleOnChange}
            ></textarea>
          </div>
          <form onSubmit={this.onSubmit}>
            <button type="submit" className="btn btn-primary">
            
          {this.props.match.params.id ? <h5>Actualizar Nota</h5> : <h5>Guardar Nota</h5>}

            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default CreateNote;
