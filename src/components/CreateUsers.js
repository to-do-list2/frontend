import React, { Component } from "react";
import axios from "axios";

class CreateUsers extends Component {
  state = {
    users: [],
    username: ""
  };
  async componentDidMount() {
    this.getUsers();
  }
  handleOnchange = e => {
    this.setState({
      username: e.target.value
    });
  };

  getUsers = async () => {
    const res = await axios.get("http://localhost:4000/api/users");
    this.setState({ users: res.data });
  };

  handleSubmit = async e => {
    e.preventDefault();
    const user = {
      username: this.state.username
    }
    console.log(user)
    await axios.post("http://localhost:4000/api/users", user);
    this.setState({ username: "" });
    this.getUsers();
  };

  deletUsers = async id => {
    await axios.delete("http://localhost:4000/api/users/" + id);
    this.getUsers();
  };

  render() {
    return (
      <div className="row">
        <div className="col-md4">
          <div className="car car-body">
            <h3>Crear Usuarios</h3>
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  value={this.state.username}
                  onChange={this.handleOnchange}
                ></input>
              </div>
              <button type="submit" className="btn btn-primary">
                Saved
              </button>
            </form>
          </div>
        </div>
        <div className="col-md-8">
          <ul className="list-group">
            {this.state.users.map(user => (
              <li
                className="list-group-item list-group-item-action"
                key={user._id}
                onDoubleClick={() => this.deletUsers(user._id)}
              >
                {user.username}
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default CreateUsers;
