import React, { Component } from "react";
import axios from "axios";

class NoteList extends Component {
  state = {
    notes: []
  };
  async componentDidMount() {
    this.getNotes();
  }

  async getNotes() {
    const res = await axios.get("http://localhost:4000/api/notes");
    this.setState({ notes: res.data });
  }

  handleClick = async note => {
    await axios.delete("http://localhost:4000/api/notes/" + note._id);
    this.getNotes();
  };

  handleUpdate= async note => {
    window.location.href = `/edit/${note._id}`;
  
  };


  render() {
    return (
      <div className="row">
        {this.state.notes.map(note => (
          <div className="col-md-4 p-2" key={note._id}>
            <div className="card">
              <div className="card-header">
                <h1 style={{color: 'black', textAlign:'center'}}>
                {note.title}
                </h1>
              </div>
              <div className="card-body">
                <div>
                <h2 style={{color: '#22222', textAlign:'center'}}>{note.content}</h2>
                </div>
                <div>
                <p style={{ marginBottom: '-15px', marginLeft: '-13px'}}><span style={{color: 'red'}}>Author:  </span>{note.author}</p>
                </div>
              </div>
              <div className="card-footer">
                <button
                  className="btn btn-danger"
                  onClick={() => this.handleClick(note)}
                >
                  Eliminar
                </button>
             <button
                  className="btn btn-secondary"
                  onClick={() => this.handleUpdate(note)}
                  style={{marginLeft: '160px'}}
                >
                  Editar
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default NoteList;
